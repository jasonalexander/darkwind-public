# DarkWind Public Repository

## Overview
This repo serves as the public interface to be shared with DarkWind's players and users publicly. Here you may find shared scripts, notes, ideas, images, anything that really could/should be shared with the playerbase at large.

